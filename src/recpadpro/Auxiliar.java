/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recpadpro;

/**
 *
 * @author helder
 */
public class Auxiliar {

    private static final int CARACTERE = 3;
    private static String resultadoFinal = "";
    static int br = 1;

    public static void fazerString(String param, String[] array, int contador) {
        if (contador != CARACTERE) {
            for (int i = 0; i < array.length; i++) {
                fazerString(param + array[i], array, (contador + 1));
            }
        } else {
            if (br % 20 != 0) {
                System.out.print("\"" + param + "\",");
                    
            } else {
                System.out.println("\"" + param + "\",");
            }
            br++;
        }

    }

    public static void main(String[] args) {
        String[] vetor = {"", "0", "1", "2", "3", "4", "a", "i", "o", "t"};
        //   String[] vetor = {"", "0", "1", "2", "3"};
        fazerString("", vetor, 0);
    }
}
