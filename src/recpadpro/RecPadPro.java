/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recpadpro;

import recpadpro.dao.ResultadoDAO;
import recpadpro.entity.Resultado;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author helder
 */
public class RecPadPro extends Thread {

    public int porcentagemMinimaTreinamento;
    public int porcentagemMaximaTreinamento;
    public static final float INCREMENTO_TAXA_APRENDIZAGEM = (float) 0.2;
    public static final float INCREMENTO_MOMENTUM_APRENDIZAGEM = (float) 0.2;
    public static final int QUANTIDADE_MAXIMA_TEMPO_TREINAMENTO = 2500;
    public static final int QUANTIDADE_MINIMA_TEMPO_TREINAMENTO = 500;
    public static final int INCREMENTO_TEMPO_TREINAMENTO = 100;
    public static final int NUMERO_NEURONIOS = 511;

    /**
     * @param args the command line arguments
     */
    public RecPadPro(int porcentagemMinimaTreinamento, int porcentagemMaximaTreinamento) {
        this.porcentagemMaximaTreinamento = porcentagemMaximaTreinamento;
        this.porcentagemMinimaTreinamento = porcentagemMinimaTreinamento;
    }

    public void run() {
        System.out.println("Thread "+this.getName());
        int loopAtual = 0;
        int totalLoop = (int) ((porcentagemMinimaTreinamento - porcentagemMaximaTreinamento)
                * (1 / INCREMENTO_TAXA_APRENDIZAGEM)
                * (1 / INCREMENTO_MOMENTUM_APRENDIZAGEM)
                * ((QUANTIDADE_MINIMA_TEMPO_TREINAMENTO - QUANTIDADE_MAXIMA_TEMPO_TREINAMENTO) / INCREMENTO_TEMPO_TREINAMENTO)
                * ((NUMERO_NEURONIOS - 100) / 50));
        for (int i = porcentagemMinimaTreinamento; i < porcentagemMaximaTreinamento; i++) {
            for (float j = (float) 0.1; j < 1; j += INCREMENTO_TAXA_APRENDIZAGEM) {
                for (float k = (float) 0.1; k < 1; k += INCREMENTO_MOMENTUM_APRENDIZAGEM) {
                    for (int l = QUANTIDADE_MINIMA_TEMPO_TREINAMENTO; l < QUANTIDADE_MAXIMA_TEMPO_TREINAMENTO; l += INCREMENTO_TEMPO_TREINAMENTO) {
                        for (int m = 10; m < NUMERO_NEURONIOS; m += 50) {
                            long tempoInicial = System.currentTimeMillis();
                            FileReader reader;
                            try {
                                reader = new FileReader("base/diabetes.arff");
                                Instances instancias = new Instances(reader);

                                Instances instanciasTreinamento = new Instances(instancias, 0, (int) ((((float) i) / 100) * (instancias.numInstances())));
                                Instances instanciasTeste = new Instances(instancias, instanciasTreinamento.numInstances(), (instancias.numInstances() - instanciasTreinamento.numInstances()));
                                instanciasTreinamento.setClassIndex(instancias.numAttributes() - 1);
                                instanciasTeste.setClassIndex(instancias.numAttributes() - 1);

                                MultilayerPerceptron mlp = new MultilayerPerceptron();
                                mlp.setAutoBuild(true);
                                mlp.setLearningRate(j);
                                mlp.setMomentum(k);
                                mlp.setTrainingTime(l);

                                mlp.setHiddenLayers(Integer.toString(m));
                                //Treinando a rede
                                mlp.buildClassifier(instanciasTreinamento);

                                int corretas = 0;
                                for (int n = 0; n < instanciasTeste.numInstances(); n++) {
                                    Instance instancia = instanciasTeste.instance(i);
                                    int classe;
                                    classe = (int) (mlp.classifyInstance(instancia));
                                    if (classe == (int) instancia.classValue()) {
                                        corretas++;
                                    }
                                }

                                loopAtual++;

                                Resultado resultado = new Resultado();
                                resultado.setInstaciaCorreta(corretas);
                                resultado.setNumeroInstancia(instancias.numInstances());
                                resultado.setTaxaAcerto((100.0 * (corretas / (1.0 * instancias.numInstances()))));
                                resultado.setPorcentagemAprendizagem(i);
                                resultado.setTaxaAprendizagem(j);
                                resultado.setMomentum(k);
                                resultado.setTempoTreinamento(l);
                                resultado.setCamadaOculta(m);

                                ResultadoDAO dao = new ResultadoDAO();
                                dao.persist(resultado);
                                System.out.println(loopAtual + " de " + totalLoop + " (" + (loopAtual / totalLoop) + "%) Tempo gasto:" + ((System.currentTimeMillis() - tempoInicial) / 1000) + "s");
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(RecPadPro.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(RecPadPro.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(RecPadPro.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                    }
                }
            }

        }
    }

    public void simpleWekaTrain(int valorMinimoTreinamento, int valorMaximoTreinamento) throws FileNotFoundException, IOException, Exception {

    }

    public static void main(String[] args) {
        RecPadPro a = new RecPadPro(70, 75);
        RecPadPro b = new RecPadPro(75, 80);
        RecPadPro c = new RecPadPro(80, 85);
        RecPadPro d = new RecPadPro(85, 90);
        a.start();
        b.start();
        c.start();
        d.start();
    }

}

