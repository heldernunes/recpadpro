package recpadpro.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import recpadpro.entity.Resultado;

/**
 *
 * @author helder
 */
public class ResultadoDAO {

    private static Session sessao;
    private Transaction transacao;
    
    private Resultado entity;
    public ResultadoDAO() {
    }

    
    public void persist(Resultado entity) {

        sessao = HibernateUtil.getSessionFactory().openSession();
        transacao = sessao.beginTransaction();
        sessao.save(entity);
        sessao.flush();
        transacao.commit();
        sessao.close();
    }

    public void update(Resultado entity) {

        sessao = HibernateUtil.getSessionFactory().openSession();
        transacao = sessao.beginTransaction();
        sessao.update(entity);
        sessao.flush();
        transacao.commit();
        sessao.close();
    }

    public void delete(Resultado entity) {
        sessao = HibernateUtil.getSessionFactory().openSession();
        transacao = sessao.beginTransaction();
        sessao.delete(entity);
        sessao.flush();
        transacao.commit();
        sessao.close();
    }

}
