package recpadpro.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author helder
 */
@Entity
public class Resultado implements Serializable{
    @Id
    @GeneratedValue
    private int id;
    private int numeroInstancia;
    private int instaciaCorreta;
    private double taxaAcerto;
    private int porcentagemAprendizagem;
    private float taxaAprendizagem;
    private float momentum;
    private int tempoTreinamento;
    private int camadaOculta;
            
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumeroInstancia() {
        return numeroInstancia;
    }

    public void setNumeroInstancia(int numeroInstancia) {
        this.numeroInstancia = numeroInstancia;
    }

    public int getInstaciaCorreta() {
        return instaciaCorreta;
    }

    public void setInstaciaCorreta(int instaciaCorreta) {
        this.instaciaCorreta = instaciaCorreta;
    }

    public double getTaxaAcerto() {
        return taxaAcerto;
    }

    public void setTaxaAcerto(double taxaAcerto) {
        this.taxaAcerto = taxaAcerto;
    }

    public int getPorcentagemAprendizagem() {
        return porcentagemAprendizagem;
    }

    public void setPorcentagemAprendizagem(int porcentagemAprendizagem) {
        this.porcentagemAprendizagem = porcentagemAprendizagem;
    }

    public float getTaxaAprendizagem() {
        return taxaAprendizagem;
    }

    public void setTaxaAprendizagem(float taxaAprendizagem) {
        this.taxaAprendizagem = taxaAprendizagem;
    }

    public float getMomentum() {
        return momentum;
    }

    public void setMomentum(float momentum) {
        this.momentum = momentum;
    }

    public int getTempoTreinamento() {
        return tempoTreinamento;
    }

    public void setTempoTreinamento(int tempoTreinamento) {
        this.tempoTreinamento = tempoTreinamento;
    }

    public int getCamadaOculta() {
        return camadaOculta;
    }

    public void setCamadaOculta(int camadaOculta) {
        this.camadaOculta = camadaOculta;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Resultado other = (Resultado) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
}
